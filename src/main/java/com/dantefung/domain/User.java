package com.dantefung.domain;

import java.io.Serializable;

import javax.persistence.*;

/**
 * User
 * @author LEE.SIU.WAH
 * @email lixiaohua7@163.com
 * @date 2015-12-19 下午2:40:32
 * @version 1.0
 */
@Entity// 持久化类
@Table(name="USER_INFO")
public class User implements Serializable {
	
	private static final long serialVersionUID = 4490820637169448834L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="U_ID")
	private int id;
	@Column(name="U_NAME", length=50)
	private String name;
	private int age;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
}
