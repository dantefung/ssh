package com.dantefung.dao.user;

import com.dantefung.dao.HibernateDao;

/**
 * UserDao.java
 * @author LEE.SIU.WAH
 * @email lixiaohua7@163.com
 * @date 2015-12-20 下午4:24:24
 * @version 1.0
 */
public interface UserDao extends HibernateDao {

}
