package com.dantefung.dao.user.impl;

import com.dantefung.dao.impl.HibernateDaoImpl;
import com.dantefung.dao.user.UserDao;

/**
 * UserDaoImpl
 * @author LEE.SIU.WAH
 * @email lixiaohua7@163.com
 * @date 2015-12-20 下午4:24:45
 * @version 1.0
 */
public class UserDaoImpl extends HibernateDaoImpl implements UserDao {

}
