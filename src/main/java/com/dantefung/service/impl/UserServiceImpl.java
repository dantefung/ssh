package com.dantefung.service.impl;

import java.util.List;

import com.dantefung.dao.user.UserDao;
import com.dantefung.domain.User;
import com.dantefung.service.UserService;

/**
 * UserServiceImpl
 * @author LEE.SIU.WAH
 * @email lixiaohua7@163.com
 * @date 2015-12-20 下午4:25:20
 * @version 1.0
 */
public class UserServiceImpl implements UserService {
	private UserDao userDao;
	
	
	public List<User> getUser(){
		return userDao.find(User.class);
	}
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
}
