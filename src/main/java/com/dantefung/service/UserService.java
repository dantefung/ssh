package com.dantefung.service;

import java.util.List;

import com.dantefung.domain.User;

/**
 * UserService.java
 * @author LEE.SIU.WAH
 * @email lixiaohua7@163.com
 * @date 2015-12-20 下午4:25:00
 * @version 1.0
 */
public interface UserService {

	List<User> getUser();

}
