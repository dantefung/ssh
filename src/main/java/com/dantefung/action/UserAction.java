package com.dantefung.action;

import java.util.List;

import com.dantefung.domain.User;
import com.dantefung.service.UserService;
import com.opensymphony.xwork2.ActionSupport;

/**
 * UserAction
 * @author LEE.SIU.WAH
 * @email lixiaohua7@163.com
 * @date 2015-12-20 下午4:26:11
 * @version 1.0
 */
public class UserAction extends ActionSupport {
	private UserService userService;
	
	@Override
	public String execute() throws Exception {
		System.out.println(userService);
		List<User> users = userService.getUser();
		System.out.println(users);
		System.out.println("==4444===");
		return SUCCESS;
	}
	

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
}
